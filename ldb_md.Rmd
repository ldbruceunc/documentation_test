---
title: "Links and Information"
knit: (function(input_file, encoding){
  out_dir <- 'docs';
  rmarkdown::render(input_file, encoding=encoding, output_file=file.path(dirname(input_file), out_dir, 'index.html'))})
author: "LD Bruce"
date: "`r Sys.Date()`"
documentclass: krantz
lot: yes
lof: yes
fontsize: 12pt
monofont: "Source Code Pro"
monofontoptions: "Scale=0.7"
output: 
  html_document:
    toc: TRUE
    toc_float: TRUE
    toc_collapsed: TRUE
    toc_depth: 3
editor_options:
  chunk_output_type: inline
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#some commented things taken out of the yaml before lot:
#bibliography: steroids_covid.bib
#biblio-style: apalike
#link-citations: yes
#colorlinks: yes

#brackets for showing the URL see <http://rmarkdown.rstudio.com>
#show code inline with `echo = FALSE`
```

## Useful Locations {.tabset}

### Administrative

[Sprint Summaries](https://adminliveunc.sharepoint.com/:f:/r/sites/ShepsAnalyticalProgrammers/Shared%20Documents/General/Sprint?csf=1&web=1&e=SLz4Lu)

[Connect Carolina](https://connectcarolina.unc.edu/)

[Harvest Timesheet](https://sirs.harvestapp.com/time/week)

[Verizon Phone Calling](https://mblogin.verizonwireless.com/amserver/sso/login.go?SetLang=en&goto=https://icp.verizonbusiness.com/integratedcommunicationspackage/login/logincheck.aspx?lang=en-US)

[TIM Timesheet](https://unctim.unc.edu/wfcstatic/applications/navigator/html5/dist/container/index.html?version=8.1.6.2033#/)

[Performance Review](https://hr.unc.edu/carolinatalent/performance/)

### Directories

[My OneDrive](https://adminliveunc-my.sharepoint.com/personal/liana_ad_unc_edu/_layouts/15/onedrive.aspx)

[My Git repos - all](https://bitbucket.org/shepsweb/workspace/projects/LBRUCE)

[JupyterLab](https://n1.schsr.unc.edu/jupyter/user/liana/lab)

### Library links

1) [UNC E-Journal Finder](https://hsl.lib.unc.edu/resources/ejsearch)
 
2) PubMed - use a [UNC portal](https://www.ncbi.nlm.nih.gov/pubmed?otool=uncchlib)

3) [NC LIVE](http://www.nclive.org/)

4) [CUFTS Journal Search](http://researcher.sfu.ca/cufts/cjdb) will tell you where particular journals are indexed
and available full-text; many of these are available through NCLIVE.
 
5) [Free Medical Journals](http://www.freemedicaljournals.com/)
 
Sheps Center Library "In-house seminars" links:
https://intranet.shepscenter.unc.edu/services/library-home/library-information-services/
 
Health Sciences Library tutorials:
http://guides.lib.unc.edu/index.php?gid=1226/
 
Here are some other helpful links:

1)	Comparison chart for bibliographic management products: http://guides.lib.unc.edu/comparecitationmanagers
2)	Mendeley tutorial: http://guides.lib.unc.edu/mendeley
3)	You can also register for various classes at HSL: http://www.hsl.unc.edu/classes?category=citation



## Projects {.tabset}

### Medicaid Waiver

[Documentation for Waiver SUD Measures](https://adminliveunc.sharepoint.com/:b:/r/sites/1115waivereval/Shared%20Documents/Quantitative/technical%20specifications%20for%20measures/SUD%20Monitoring%20Tech%20Specs%20v.3%20Aug%202020/1115_SUD_TechSpecsManual_v3.pdf?csf=1&web=1&e=PjlRXn)

[Bruce Exploratory / M3 git ](https://bitbucket.org/lbruceUNC/mcaid-waiver-lb/src/master/explor_1.sas)

[Bruce codelists for M3](https://bitbucket.org/lbruceUNC/mcaid-waiver-lb/src/master/codesets_m3.sas)

[Excel file Value Set Programs.xlsx](https://adminliveunc.sharepoint.com/sites/1115waivereval/Shared Documents/Quantitative/Metrics/)

### COVID MAST

[Bruce COVID MAST git](https://bitbucket.org/lbruceUNC/covid-mast/src/master/)

### ACA Comparator

[Bruce ACA git](https://bitbucket.org/lbruceUNC/acacomparison/src/master/)

### Provider Mail Survey

[Bruce Provider Mail Survey git](https://bitbucket.org/lbruceUNC/providermail/src/master/)

